I was inspired by my childhood memories where there are so many traditional games I was in to celebrate national independence day. The games are quite simple, but we were so enthusiastic in playing it. Here I want to bring such concept where people just need to tap on their screen to win the game! No login data required, just tap!

The second core concept is that traditional games are played together on the playground. With Balap Jari you have that thing, playing together on one device.

Anyway, this games are built with React JS and Tailwind CSS. I deployed it on https://balapjari.herokuapp.com/