import React, { useState, useEffect } from 'react';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import '../App.css'
import useSound from 'use-sound';
import ingameSound from '../assets/ingame.mp3';

const InGame = () => {
    const [redBlood, setRedBlood] = useState(window.innerHeight / 2);
    const [whiteBlood, setWhiteBlood] = useState(window.innerHeight / 2);
    const [play, { stop }] = useSound(ingameSound);
    const redAttack = () => {
        console.log("red attacked")
        setRedBlood(redBlood + 20);
        setWhiteBlood(whiteBlood - 20);
        window.navigator.vibrate(200);
        if (redBlood >= window.innerHeight) {
            toast.success("Si Merah Menang!");
            setRedBlood(window.innerHeight / 2);
            setWhiteBlood(window.innerHeight / 2);
        }
    }

    const whiteAttack = () => {
        console.log("white attacked")
        setRedBlood(redBlood - 20);
        setWhiteBlood(whiteBlood + 20);
        window.navigator.vibrate(200);
        if (whiteBlood >= window.innerHeight) {
            toast.success("Si Putih Menang!");
            setRedBlood(window.innerHeight / 2);
            setWhiteBlood(window.innerHeight / 2);
        }
    }

    return (
        <div className="">
            <div onClick={redAttack} className={`bg-red-500 flex justify-between`} style={{ height: redBlood }}>
                <h1 className="text-white p-5 font-semibold">Si Merah</h1>
                <h1 className="text-white p-5 font-semibold">{ (redBlood / window.innerHeight * 100).toFixed() } %</h1>
            </div>

            <div onClick={whiteAttack} className={`bg-white flex justify-between`} style={{ height: whiteBlood }}>
                <h1 className="text-red-500 p-5 font-semibold">Si Putih</h1>
                <h1 className="text-red-500 p-5 font-semibold">{ (whiteBlood / window.innerHeight * 100).toFixed() } %</h1>
            </div>
            <ToastContainer/>
        </div>
    );
}

export default InGame;
