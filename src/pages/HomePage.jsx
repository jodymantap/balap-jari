import React, { useEffect } from 'react';
import clickIt from '../assets/click.gif'
import balap from '../assets/balap.png'
import title from '../assets/title.png'
import { useHistory } from 'react-router';


const HomePage = () => {
    const history = useHistory();
    const startGame = () => {
        history.push("/ingame");
    }
    
    return (
        <div onClick={startGame} className="bg-red-500 h-screen app-container">
            {/* header */}
            <div className="flex justify-center">
                <img className="mt-10" width="200" src={title} alt="title" />
            </div>

            <div className="flex justify-center">
                <img className="mt-20" width="300" src={balap} alt="balap" />
            </div>

            <div className="flex justify-center">
                <img width="120" src={clickIt} alt="loading..." />
            </div>

            <div className="flex justify-center">
                <p className="text-white">
                    Ketuk untuk mulai ...
                </p>
            </div>
        </div>
    );
}

export default HomePage;
