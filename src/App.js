import logo from './logo.svg';
import HomePage from './pages/HomePage';
import InGame from './pages/InGame';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";


function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={HomePage} />
        <Route exact path="/ingame" component={InGame} />
      </Switch>
    </Router>
  );
}

export default App;
